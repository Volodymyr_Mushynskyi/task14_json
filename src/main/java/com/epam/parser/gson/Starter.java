package com.epam.parser.gson;

import com.epam.model.Plane;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

public class Starter {

  private static final Type REVIEW_TYPE = new TypeToken<List<Plane>>() {}.getType();
  private static final Logger logger = LogManager.getLogger(Starter.class);

  public static void main(String[] args) throws IOException {
    Gson gson = new Gson();
    try(Reader reader = new FileReader("D:\\QA_projects\\task14_JSON\\src\\main\\resources\\aircraft.json")){
      List<Plane> planes = gson.fromJson(reader, REVIEW_TYPE);
      logger.info(planes);
    }
  }
}
