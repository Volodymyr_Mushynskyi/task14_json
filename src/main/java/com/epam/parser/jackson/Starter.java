package com.epam.parser.jackson;

import com.epam.model.Plane;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

public class Starter {

  private static final Logger logger = LogManager.getLogger(Starter.class);

  public static void main(String[] args) throws IOException {

    ObjectMapper mapper = new ObjectMapper();
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    try (Reader reader = new FileReader("D:\\QA_projects\\task14_JSON\\src\\main\\resources\\aircraft.json")) {
      List<Plane> planes = Arrays.asList(mapper.readValue(reader, Plane[].class));
      logger.info(planes);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
