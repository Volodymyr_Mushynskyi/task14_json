package com.epam.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Chars {
  private String type;
  private Integer seater;
  private Ammunition ammunition;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getSeater() {
    return seater;
  }

  public void setSeater(Integer seater) {
    this.seater = seater;
  }

  public Ammunition getAmmunition() {
    return ammunition;
  }

  public void setAmmunition(Ammunition ammunition) {
    this.ammunition = ammunition;
  }

  @Override
  public String toString() {
    return "Chars{" +
            "type='" + type + '\'' +
            ", seater='" + seater + '\'' +
            ", ammunition=" + ammunition +
            '}';
  }
}
