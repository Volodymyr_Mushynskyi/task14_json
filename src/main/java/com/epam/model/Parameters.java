package com.epam.model;

public class Parameters {
  private Double length;
  private Double width;
  private Double height;

  public Double getLength() {
    return length;
  }

  public void setLength(Double length) {
    this.length = length;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  @Override
  public String toString() {
    return "Parameters{" +
            "length=" + length +
            ", width=" + width +
            ", height=" + height +
            '}';
  }
}
