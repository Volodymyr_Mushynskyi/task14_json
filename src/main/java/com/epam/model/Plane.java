package com.epam.model;

import com.google.gson.annotations.SerializedName;

public class Plane {
  @SerializedName("id")
  private Integer id;
  @SerializedName("model")
  private String model;
  @SerializedName("origin")
  private String origin;
  @SerializedName("chars")
  private Chars chars;
  @SerializedName("parameters")
  private Parameters parameters;
  @SerializedName("price")
  private Double price;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public Chars getChars() {
    return chars;
  }

  public void setChars(Chars chars) {
    this.chars = chars;
  }

  public Parameters getParameters() {
    return parameters;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "Plane{" +
            "id=" + id +
            ", model=" + model +
            ", origin='" + origin + '\'' +
            ", chars=" + chars +
            ", parameters=" + parameters +
            ", price=" + price +
            '}';
  }
}
